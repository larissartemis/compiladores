QT += core
QT -= gui

CONFIG += c++11

TARGET = Trabalho-2
CONFIG += console
CONFIG -= app_bundle

TEMPLATE = app

SOURCES += main.cpp \
    handlefile.cpp \
    token.cpp

HEADERS += \
    handlefile.h \
    token.h

DISTFILES += \
    Trabalho-2.pro.user \
    fibonacci.txt \
    helloWorld.txt \
    shellSort.txt \
    to-do.txt
