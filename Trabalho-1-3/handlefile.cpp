#include "handlefile.h"

handleFile::handleFile(QString path)
{
    m_path = path;
    m_separators.append('(');
    m_separators.append(')');
    m_separators.append('{');
    m_separators.append('}');
    m_separators.append(',');
    m_separators.append(';');
    m_separators.append('=');
    m_separators.append('>');
    m_separators.append('<');
    m_separators.append('+');
    m_separators.append('-');
    m_separators.append('*');
    m_separators.append('/');
    m_separators.append('^');
    m_separators.append('%');
}

bool handleFile::readFile()
{
    QFile file(m_path);
    if (!file.open(QIODevice::ReadOnly | QIODevice::Text)) {
        return false;
    }

    int numberLine = 0;
    while (!file.atEnd()) {
        QByteArray line = file.readLine();
        if (!line.isNull()) {
            numberLine++;
            processLine(line.trimmed (), numberLine);
        }
    }

    file.close();
    return true;
}


void handleFile::processLine(QByteArray line, int numberLine)
{
    int lastSeparator = 0;
    line = line.trimmed();
    QString aux = QString (line);

    for (int i = 0; i < line.length(); i++) {
        if (line.startsWith("//") || (line.at(i) == '/' && line.at(i+1) == '/')) {
            // comentário
            break;
        }

        // add tokens numa pilha
        QString lexema;
        foreach (char s, m_separators) {
            if (line.at(i) == '\"') {
                // aspas duplas
                QRegularExpression ourString ("\"[\\s\\S]*\"");
                QRegularExpressionMatch matchedStr = ourString.match(line);
                if (matchedStr.hasMatch ()) {
                    lexema = matchedStr.captured();
                    int index = matchedStr.capturedEnd() + 1;

                    m_tokens.append(new Token (numberLine, i, lexema));

                    i = index;
                    lastSeparator = index;
                } else {
                    qDebug() << "ERRO" << numberLine;
                    exit(0);
                }
                break;
            } else if (line.at(i) == '\'') {
                int index = aux.indexOf('\'', i + 1);
                lexema = aux.mid(i + 1, index - i - 1).trimmed();

                m_tokens.append(new Token (numberLine, i, lexema));

                i = index;
                lastSeparator = index;

                break;
            } else if (line.at(i) == ' ') {
                if (lastSeparator == (i - 1)) {
                    // espaço
                    lastSeparator = i;

                    break;
                } else if (lastSeparator == 0) {
                    // primeiro espaço lido na linha
                    lexema = aux.left(i).trimmed();

                    m_tokens.append(new Token (numberLine, lastSeparator, lexema));
                } else {
                    // texto entre um separador e o espaço lido agora
                    lexema = aux.mid(lastSeparator + 1, i - lastSeparator - 1).trimmed();

                    m_tokens.append(new Token (numberLine, lastSeparator + 1, lexema));
                }
                lastSeparator = i;

                break;
            } else if (line.at(i) == s) {
                if (lastSeparator >= (i - 1)) {
                    // acima, > é justificado pelo caso do primeiro caracter ser um separador, como }
                    // já = é justificado pela possibilidade de 2 separadores seguidos, como ) {
                    lexema = QString (line.at(i));
                    lastSeparator = i;

                    m_tokens.append(new Token (numberLine, i, lexema));
                } else {
                    if (lastSeparator == 0) {
                        // primeiro separador lido na linha
                        lexema = aux.left(i).trimmed();

                        m_tokens.append(new Token (numberLine, lastSeparator, lexema));
                        m_tokens.append(new Token (numberLine, i, QString(line.at(i))));
                    } else {
                        // texto entre o separador anterior e o separador lido agora
                        lexema = aux.mid(lastSeparator + 1, i - lastSeparator - 1).trimmed();

                        m_tokens.append(new Token (numberLine, lastSeparator + 1, lexema));
                        m_tokens.append(new Token (numberLine, i, QString(line.at(i))));
                    }
                    lastSeparator = i;
                }
            }
        }
    }
}

Token* handleFile::nextToken()
{
    m_count++;
    return m_tokens.at (m_count);
}

Token *handleFile::prevToken()
{
    m_count--;
    return m_tokens.at (m_count);
}

QString handleFile::path()
{
    return m_path;
}

void handleFile::setPath(QString newPath)
{
    m_path = newPath;
}
