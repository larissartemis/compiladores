#ifndef HANDLEFILE_H
#define HANDLEFILE_H

#include "token.h"
#include <QString>
#include <QFile>
#include <QList>
#include <QRegularExpressionMatch>
#include <QRegularExpression>

class handleFile
{
public:
    handleFile(QString path);
    ~handleFile();

    Token* nextToken();
    Token* prevToken();

    QString path();
    void setPath(QString newPath);

    void processLine(QByteArray line, int numberLine);
    bool readFile();

private:
    QString m_path;
    QList <char> m_separators;

    QList <Token *> m_tokens;
    int m_count = 0;

    int m_lastSeparator = 0;
    int m_nLine, m_nColumn = 0;
};

#endif // READFILE_H
