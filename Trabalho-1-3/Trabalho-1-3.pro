QT += core
QT -= gui

CONFIG += c++11

TARGET = Trabalho-1-3
CONFIG += console

TEMPLATE = app

SOURCES += main.cpp \
    token.cpp \
    handlefile.cpp

DISTFILES += \
    teste.txt \
    TO_DO.docx \
    to-do.txt \
    helloWorld.txt \
    shellSort.txt \
    fibonacci.txt \
    outputSS.txt \
    outputF.txt \
    outputHW.txt

HEADERS += \
    token.h \
    handlefile.h
