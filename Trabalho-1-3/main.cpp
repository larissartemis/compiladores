#include <QDebug>
#include <QCoreApplication>

#include "handlefile.h"

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);

    QString path = "/home/larissa/compiladores/Trabalho-1-3/helloWorld.txt";
    // QString path = "/home/larissa/compiladores/Trabalho-1-3/fibonacci.txt";
    // QString path = "/home/larissa/compiladores/Trabalho-1-3/shellSort.txt";
    handleFile *hf = new handleFile();
    hf->setPath(path);

    Token * tk = hf->nextToken();
    while (tk->lexema() != "EOF") {
        tk = hf->nextToken();
    }

    qDebug() << "done.";

    return a.exec();
}
