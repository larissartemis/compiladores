#include "token.h"

Token::Token()
{

}

Token::Token(int nLine, int nColumn, QString nLexema)
{
    m_line = nLine;
    m_column = nColumn;
    m_lexema = nLexema;
    m_category = identifier();
    print();
}

Token::~Token()
{
    m_line = 0;
    m_column = 0;
    m_lexema = "";
}

void Token::print()
{
    qDebug() << "Token >> valor léxico" << m_lexema << "categoria"
             << m_category << "posição" << m_line << m_column;
}

Token::tokenCateg Token::identifier()
{
    QRegExp reInt("\\d*");
    QRegExp reFloat("\\d*.\\d*");
    QRegExp reId("[A-Z|a-z][\\w|\\d]*");

    if (m_lexema == "==" || m_lexema == "=!" || m_lexema == ">"||
            m_lexema == "<"|| m_lexema == "=>" || m_lexema == "=<")
        return Token::opRel;
    else if (m_lexema == "+" || m_lexema == "-") return Token::opAd;
    else if (m_lexema == "--") return Token::opUn;
    else if (m_lexema == "*" || m_lexema == "/") return Token::opMult;
    else if (m_lexema == "^" || m_lexema == "%") return Token::opExp;
    else if (m_lexema == "AND" || m_lexema == "OR") return Token::opBool;
    else if (m_lexema == "++") return Token::concat;
    else if (m_lexema == "#cast") return Token::prCast;
    else if (m_lexema == "~int") return Token::prInt;
    else if (m_lexema == "~longInt") return Token::prLInt;
    else if (m_lexema == "~float") return Token::prFloat;
    else if (m_lexema == "~char") return Token::prChar;
    else if (m_lexema == "~string") return Token::prStr;
    else if (m_lexema == "~bool") return Token::prBool;
    else if (m_lexema == "~array") return Token::prArray;
    else if (m_lexema == "~void") return Token::prVoid;
    else if (m_lexema == "#if") return Token::prIf;
    else if (m_lexema == "#else") return Token::prElse;
    else if (m_lexema == "#while") return Token::prWhile;
    else if (m_lexema == "#for") return Token::prFor;
    else if (m_lexema == "#read") return Token::prRead;
    else if (m_lexema == "#write") return Token::prWrite;
    else if (m_lexema == "true") return Token::prTrue;
    else if (m_lexema == "false") return Token::prFalse;
    else if (m_lexema == "main") return Token::prMain;
    else if (m_lexema == "(") return Token::srParO;
    else if (m_lexema == ")") return Token::srParC;
    else if (m_lexema == "[") return Token::srColO;
    else if (m_lexema == "]") return Token::srColC;
    else if (m_lexema == "{") return Token::srChaO;
    else if (m_lexema == "}") return Token::srChaC;
    else if (m_lexema == "=") return Token::srAtr;
    else if (m_lexema == ";") return Token::srSc;
    else if (m_lexema == ",") return Token::srCommu;
    else if (m_lexema.startsWith("$")) return Token::prIdFunc;
    else if (m_lexema.startsWith("\"")) return Token::cteStr;
    else if (m_lexema.startsWith("\'")) return Token::cteChar;
    else if (reInt.exactMatch(m_lexema)) return Token::cteInt;
    else if (reFloat.exactMatch(m_lexema)) return Token::cteFloat;
    else if (reId.exactMatch(m_lexema)) return Token::prId;
    else return Token::invalid;
}

int Token::line()
{
    return m_line;
}

int Token::colunm()
{
    return m_column;
}

Token::tokenCateg Token::category()
{
    return m_category;
}

QString Token::lexema()
{
    return m_lexema;
}

void Token::setLine(int nLine)
{
    m_line = nLine;
}

void Token::setColumn(int nColumn)
{
    m_column = nColumn;
}

void Token::setCategory(Token::tokenCateg nCategory)
{
    m_category = nCategory;
}

void Token::setLexema(int nLexema)
{
    m_lexema = nLexema;
}
