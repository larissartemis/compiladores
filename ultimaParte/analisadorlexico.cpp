#include "analisadorlexico.h"
#include "handlefile.h"

#include <iostream>

analisadorLexico::analisadorLexico(QString arquivo)
{
    tk = new Token();
    hf = new handleFile(arquivo);
    hf->readFile();
}

void analisadorLexico::nossoMain() {
    tk = hf->nextToken();
    tk = hf->prevToken();

    if (tk->category() == Token::tokenCateg::prVoid) {
        tk = hf->nextToken();
        if (tk->category() == Token::tokenCateg::prMain) {
            tk = hf->nextToken();
            if (tk->category() == Token::tokenCateg::srParO) {
                tk = hf->nextToken();
                if (tk->category() == Token::tokenCateg::srParC) {
                    tk = hf->nextToken();
                    if (tk->category() == Token::tokenCateg::srChaO) {
                        std::cout << "main (" << nossoContador() << ") = 'prVoid' (~void) 'prMain' (main) ";
                        std::cout << "'srParO' (() 'srParC' ()) 'srChaO' ({) ls (" << nossoContador()
                                  << ") 'srChaC' (})\n";
                        nossoLs();
                        qDebug() << "----------------------------------------------------------";
                        qDebug() << "Nao foi identificado nenhum erro :)";
                        qDebug() << "----------------------------------------------------------";
                        qDebug() << "Feito.";
                    } else {
                        erro ("falta {");
                    }
                } else {
                    erro("falta )");
                }
            } else {
                erro ("falta (");
            }
        } else {
            erro ("main esperado");
        }
    } else {
        erro ("void esperado");
    }
}

void analisadorLexico::nossoS()
{
    if (tk->category() == Token::tokenCateg::prIf) {
        std::cout << "ls (" << count << ") = if (";
        std::cout << nossoContador() << ")\n";
        nossoIf();
    } else if (tk->category() == Token::tokenCateg::prRead) {
        std::cout << "ls (" << count << ") = read (";
        std::cout << nossoContador() << ")\n";
        nossoRead();
    } else if (tk->category() == Token::tokenCateg::prWrite) {
        std::cout << "ls (" << count << ") = write (";
        std::cout << nossoContador() << ")\n";
        nossoWrite();
    } else if (tk->category() == Token::tokenCateg::prCast) {
        std::cout << "ls (" << count << ") = cast (";
        std::cout << nossoContador() << ")\n";
        nossoCast();
    } else if (tk->category() == Token::tokenCateg::prWhile) {
        std::cout << "ls (" << count << ") = while (";
        std::cout << nossoContador() << ")\n";
        nossoWhile();
    } else if (tk->category() == Token::tokenCateg::prFor) {
        std::cout << "ls (" << count << ") = for (";
        std::cout << nossoContador() << ")\n";
        nossoFor();
    } else if (tk->category() == Token::tokenCateg::prInt || tk->category() == Token::tokenCateg::prLInt
               || tk->category() == Token::tokenCateg::prFloat || tk->category() == Token::tokenCateg::prChar ||
               tk->category() == Token::tokenCateg::prStr || tk->category() == Token::tokenCateg::prBool) {
        std::cout << "ls (" << count << ") = atrTipo (";
        std::cout << nossoContador() << ")\n";
        nossoAtrTipo();
    } else if (tk->category() == Token::tokenCateg::prArray) {
        std::cout << "ls (" << count << ") = array (";
        std::cout << nossoContador() << ")\n";
        nossoAtrArray();
    } else if (tk->category() == Token::tokenCateg::prId) {
        std::cout << "ls (" << count << ") = atr (";
        std::cout << nossoContador() << ")\n";
        nossaAtr();
    }
}

void analisadorLexico::nossoLs(){
    tk = hf->nextToken();
    nossoS();
    if (tk->category() == Token::tokenCateg::srChaC) {
        while (tk->category() == Token::tokenCateg::srChaC && hf->hasNextToken()) {
            tk = hf->nextToken();
        }
        if (!hf->hasNextToken()) {
            qDebug() << "----------------------------------------------------------";
            qDebug() << "Nao foi identificado nenhum erro :)";
            qDebug() << "----------------------------------------------------------";
            qDebug() << "Feito.";
            exit(0);
        } else {
            tk = hf->prevToken();
            nossoLs();
        }
    } else if (tk->category() != Token::tokenCateg::srSc) {
        tk = hf->nextToken();
    }
    if (tk->category() == Token::tokenCateg::srSc) {
        tk = hf->nextToken();
        if (tk->category() != Token::tokenCateg::srChaC) {
            tk = hf->prevToken();
            nossoLs();
        }
    } else {
        //        erro("falta ;");
    }
}

void analisadorLexico::nossoIf(){
    std::cout << "if (" << count << ") = 'prIf' (#if) 'srParO' (() expBool (";
    std::cout << nossoContador() << ") 'srPacC' ()) 'srChaO' ({) ls (";
    std::cout << nossoContador() << ") 'srChaC' ({)\n";
    tk = hf->nextToken();

    if (tk->category() == Token::tokenCateg::srParO) {
        nossaExpBool();
        tk = hf->nextToken();
        if (tk->category() == Token::tokenCateg::srParC) {
            tk = hf->nextToken();

            if (tk->category() == Token::tokenCateg::srChaO) {
                nossoLs();
            } else{
                erro("falta {");
            }
        } else{
            erro("falta )");
        }
    } else{
        erro("falta (");
    }
}

void analisadorLexico::nossoElse(){
    std::cout << "else (" << count << ") = 'prElse' (#else) 'srChaO' ({) ls (";
    std::cout << nossoContador() << ") 'srChaC' ({)\n";
    tk = hf->nextToken();

    if (tk->category() == Token::tokenCateg::prElse){
        tk = hf->nextToken();

        if (tk->category() == Token::tokenCateg::srChaO){
            nossoLs();

        } else{
            erro("falta {");
        }
    }
}


void analisadorLexico::nossoRead(){
    tk = hf->nextToken();

    if (tk->category() == Token::tokenCateg::srParO){
        tk = hf->nextToken();

        QString auxLex = tk->lexema();
        if (tk->category() == Token::tokenCateg::prId){
            std::cout << "read (" << count << ") = 'prRead' (#read) 'srParO' (() id (";
            tk = hf->nextToken();

            if (tk->category() == Token::tokenCateg::srColO) {
                std::cout << auxLex.toStdString().c_str() << "[";
                tk = hf->nextToken();

                if (tk->category() == Token::tokenCateg::prId || tk->category() == Token::tokenCateg::cteInt) {
                    std::cout << tk->lexema().toStdString().c_str() << "]) 'srParC' ())\n";
                    tk = hf->nextToken();

                    if (tk->category() == Token::tokenCateg::srColC) {
                        tk = hf->nextToken();
                    } else {
                        erro ("falta ]");
                    }
                } else {
                    erro ("id ou inteiro esperado");
                }
            } else {
                std::cout << auxLex.toStdString().c_str() << ") 'srParC' ())\n";
            }

            if (tk->category() != Token::tokenCateg::srParC){
                erro("falta )");
            }
        } else {
            erro("falta id");
        }
    } else {
        erro ("falta )");
    }
}

void analisadorLexico::nossoWrite(){
    tk = hf->nextToken();

    if (tk->category() == Token::tokenCateg::srParO){
        std::cout << "write (" << count << ") = 'prWrite' (#write) 'srParO' (() varValor (";
        std::cout << nossoContador() << ") 'srParC' ())\n";
        nossoVarValor();

        tk = hf->nextToken();
        if (tk->category() != Token::tokenCateg::srParC){
            erro("falta )");
        }
    } else {
        erro("falta (");
    }
}

void analisadorLexico::nossoCast(){
    tk = hf->nextToken();

    if (tk->category() == Token::tokenCateg::srParO){
        std::cout << "cast (" << count << ") = 'prCast' (#cast) 'srParO' (() varValor (";
        std::cout << nossoContador() << ") 'srParC' ())\n";
        nossoVarValor();

        tk = hf->nextToken();
        if (tk->category() == Token::tokenCateg::srComma){
            nossoTipo();

            tk = hf->nextToken();
            if (tk->category() == Token::tokenCateg::srComma){
                tk = hf->nextToken();

                if (tk->category() == Token::tokenCateg::prId){
                    tk = hf->nextToken();

                    if (tk->category() != Token::tokenCateg::srParC){
                        erro("falta )");
                    }
                } else {
                    erro("falta id");
                }
            } else {
                erro("falta ,");
            }
        } else {
            erro("falta ,");
        }
    } else {
        erro("falta (");
    }
}

void analisadorLexico::nossoVarValor(){
    std::cout << "varValor (" << count << ") = ";
    tk = hf->nextToken();

    if (tk->category() == Token::tokenCateg::prId) {
        std::cout << "'id' (" << tk->lexema().toStdString().c_str() << ")";
    } else if (tk->category() == Token::tokenCateg::cteStr) {
        std::cout << "'cteStr' (" << tk->lexema().toStdString().c_str() << ")";
    } else if (tk->category() == Token::tokenCateg::cteChar) {
        std::cout << "'cteChar' (" << tk->lexema().toStdString().c_str() << ")";
    } else if (tk->category() == Token::tokenCateg::cteInt) {
        std::cout << "'cteInt' (" << tk->lexema().toStdString().c_str() << ")";
    } else if (tk->category() == Token::tokenCateg::cteFloat) {
        std::cout << "'cteFloat' (" << tk->lexema().toStdString().c_str() << ")";
    } else if (tk->category() == Token::tokenCateg::prTrue) {
        std::cout << "'prTrue' (" << tk->lexema().toStdString().c_str() << ")";
    } else if (tk->category() == Token::tokenCateg::prFalse) {
        std::cout << "'prFalse' (" << tk->lexema().toStdString().c_str() << ")";
    } else {
        erro ("falta um id ou valor");
    }

    tk = hf->nextToken();

    if (tk->category() == Token::tokenCateg::srComma){
        std::cout << ", varValor (" << nossoContador() << ")\n";
        nossoVarValor();
    } else {
        std::cout << "\n";
        tk = hf->prevToken();
    }
}

void analisadorLexico::nossoTipo(){
    tk = hf->nextToken();

    if (tk->category() == Token::tokenCateg::prInt) {
        std::cout << "tipo (" << count << ") = 'prInt' (~int) \n";
    } else if (tk->category() == Token::tokenCateg::prLInt) {
        std::cout << "tipo (" << count << ") = 'prLInt' (~lint) \n";
    } else if (tk->category() == Token::tokenCateg::prFloat) {
        std::cout << "tipo (" << count << ") = 'prFloat' (~float) \n";
    } else if (tk->category() == Token::tokenCateg::prChar) {
        std::cout << "tipo (" << count << ") = 'prChar' (~char) \n";
    } else if (tk->category() == Token::tokenCateg::prStr) {
        std::cout << "tipo (" << count << ") = 'prStr' (~string) \n";
    } else if (tk->category() == Token::tokenCateg::prBool) {
        std::cout << "tipo (" << count << ") = 'prBool' (~bool) \n";
    } else {
        erro ("tipo inválido");
    }
}

void analisadorLexico::nossoWhile(){
    std::cout << "while (" << count << ") = 'prWhile' (#while) 'srParO' (() expBool (";
    std::cout << nossoContador() << ") 'srParC' ()) 'srChaO' ({) ls (";
    std::cout << nossoContador() << ") 'srChaC' (})\n";
    tk = hf->nextToken();

    if (tk->category() == Token::tokenCateg::srParO){
        nossaExpBool();

        tk = hf->nextToken();
        if (tk->category() == Token::tokenCateg::srParC){
            tk = hf->nextToken();

            if (tk->category() == Token::tokenCateg::srChaO){
                nossoLs();

            } else{
                erro("falta {");
            }
        } else {
            erro("falta )");
        }
    } else {
        erro("falta (");
    }
}

void analisadorLexico::nossoFor(){
    std::cout << "for (" << count << ") = 'prFor' (#for) 'atr' (";
    std::cout << nossoContador() << ") 'srSc' (;) expRel (";
    std::cout << nossoContador() << ") 'srSc' (;) expArit (";
    std::cout << nossoContador() << ") 'srChaO' ({) ls (";
    std::cout << nossoContador() << ") 'srChaC' (})\n";
    tk = hf->nextToken();

    if (tk->category() == Token::tokenCateg::srParO){
        nossoTipo();
        nossoAtrTipo();

        tk = hf->nextToken();
        if (tk->category() == Token::tokenCateg::srSc){
            nossaExpRel();

            tk = hf->nextToken();
            if (tk->category() == Token::tokenCateg::srSc){
                tk = hf->nextToken();
                nossaAtr();

                if (tk->category() == Token::tokenCateg::srParC){
                    tk = hf->nextToken();

                    if (tk->category() == Token::tokenCateg::srChaO){
                        nossoLs();

                    } else {
                        erro("falta {");
                    }
                } else {
                    erro("falta )");
                }
            } else {
                erro("falta ;");
            }
        } else {
            erro("falta ;");
        }
    } else {
        erro("falta (");
    }
}

void analisadorLexico::nossoConcat(){
    std::cout << "concat (" << count << ") = ";
    tk = hf->nextToken();

    if (tk->category() == Token::tokenCateg::cteStr){
        std::cout << "'cte_string' (" << tk->lexema().toStdString().c_str() << ")";
        tk = hf->nextToken();

        if (tk->category() == Token::tokenCateg::concat){
            std::cout << "concat (" << nossoContador() << ")\n";
            nossoConcat();
        } else {
            std::cout << "\n";
            tk = hf->prevToken();
            // Sai do concat
        }
    } else{
        erro("não é uma string");
    }

}

void analisadorLexico::nossoAtrTipo(){
    tk = hf->nextToken();

    if (tk->category() == Token::tokenCateg::prId){
        std::cout << "atrTipo (" << count << ") = atr (";
        std::cout << nossoContador() << ")\n";
        nossaAtr();
    } else {
        erro("falta id");
    }
}

void analisadorLexico::nossaAtr(){
    tk = hf->nextToken();

    if (tk->category() == Token::tokenCateg::srAtr){
        tk = hf->nextToken();
        tk = hf->nextToken();

        if (tk->category() == Token::tokenCateg::opAd || tk->category() == Token::tokenCateg::opMult
                || tk->category() == Token::tokenCateg::opExp || tk->category() == Token::tokenCateg::opBool
                || tk->category() == Token::tokenCateg::opRel || tk->category() == Token::tokenCateg::opUn) {
            std::cout << "atr (" << count << ") = expArit (";
            std::cout << nossoContador() << ")\n";

            tk = hf->prevToken();
            tk = hf->prevToken();
            nossaExpBool();
        } else if (tk->category() == Token::tokenCateg::srColO) {
            std::cout << "atr (" << count << ") = valorArray (";
            std::cout << nossoContador() << ")\n";

            nossoValor();
            tk = hf->nextToken();
            if (tk->category() != Token::tokenCateg::srColC) {
                erro("falta ]");
            }
        } else {
            std::cout << "atr (" << count << ") = expArit (";
            std::cout << nossoContador() << ")\n";

            tk = hf->prevToken();
            tk = hf->prevToken();
            nossoValor();
        }
    } else if (tk->category() == Token::tokenCateg::srColO) {
        tk = hf->prevToken();
        std::cout << "atr (" << count << ") = 'id'' (" << tk->lexema().toStdString().c_str() << " [ valor (";
        std::cout << nossoContador() << ") ] )\n";
        tk = hf->nextToken();
        nossoValor();

        tk = hf->nextToken();
        if (tk->category() == Token::tokenCateg::srColC) {
            nossaAtr();
        } else {
            erro("falta ]");
        }
    } else {
        std::cout << "atr (" << count << ") = vazio \n";
        count++;
        tk = hf->prevToken();
    }
}

void analisadorLexico::nossoValor(){
    tk = hf->nextToken();

    if (tk->category() == Token::tokenCateg::cteInt) {
        std::cout << "valor (" << count << ") = 'valorInt' (" << tk->lexema().toStdString().c_str() << ")\n";
    } else if (tk->category() == Token::tokenCateg::cteFloat) {
        std::cout << "valor (" << count << ") = 'valorFloat' (" << tk->lexema().toStdString().c_str() << ")\n";
    } else if (tk->category() == Token::tokenCateg::cteChar) {
        std::cout << "valor (" << count << ") = 'valorChar' (" << tk->lexema().toStdString().c_str() << ")\n";
    } else if (tk->category() == Token::tokenCateg::cteStr) {
        std::cout << "valor (" << count << ") = 'valorStr' (" << tk->lexema().toStdString().c_str() << ")\n";
    } else if (tk->category() == Token::tokenCateg::prTrue) {
        std::cout << "valor (" << count << ") = 'prTrue' (true) \n";
    } else if (tk->category() == Token::tokenCateg::prFalse) {
        std::cout << "valor (" << count << ") = 'prFalse' (false) \n";
    } else if (tk->category() == Token::tokenCateg::prId) {
        std::cout << "valor (" << count << ") = 'prId' (" << tk->lexema().toStdString().c_str() << ") \n";
    } else {
        erro("não é um valor válido");
    }
}


void analisadorLexico::nossoAtrArray(){

    tk = hf->nextToken();

    if (tk->category() == Token::tokenCateg::prInt || tk->category() == Token::tokenCateg::prLInt
            || tk->category() == Token::tokenCateg::prFloat || tk->category() == Token::tokenCateg::prChar ||
            tk->category() == Token::tokenCateg::prStr || tk->category() == Token::tokenCateg::prBool) {
        tk = hf->nextToken();

        if (tk->category() == Token::tokenCateg::prId){
            std::cout << "atrArray (" << count << ") = 'prId' (" << tk->lexema().toStdString().c_str() << "[";
            tk = hf->nextToken();

            if (tk->category() == Token::tokenCateg::srColO){
                tk = hf->nextToken();

                if (tk->category() == Token::tokenCateg::cteInt || tk->category() == Token::tokenCateg::prId){
                    std::cout << tk->lexema().toStdString().c_str() << "])";
                    tk = hf->nextToken();

                    if (tk->category() == Token::tokenCateg::srColC){
                        tk = hf->nextToken();

                        if (tk->category() == Token::tokenCateg::srAtr){
                            tk = hf->nextToken();

                            if (tk->category() == Token::tokenCateg::srColO){
                                tk = hf->nextToken();

                                while (tk->category() != Token::tokenCateg::srColC){
                                    tk = hf->prevToken();
                                    std::cout << " = valor (" << nossoContador() << ")\n";
                                    nossoValor();
                                    tk = hf->nextToken();
                                }
                                tk = hf->prevToken();
                                //Terminou a declaração de array com atribuição

                            } else {
                                erro("falta [");
                            }
                        } else {
                            tk = hf->prevToken();
                            // Declaração de array sem atribuição
                        }
                    } else {
                        erro("falta ]");
                    }
                } else {
                    erro("não é um inteiro");
                }
            } else {
                erro("falta [");
            }
        } else {
            erro("falta id");
        }
    } else {
        erro("tipo inválido");
    }
    std::cout << "\n";
}

void analisadorLexico::nossaExpBool(){
    std::cout << "expBool (" << count << ") = termoBool (";
    std::cout << nossoContador() << ")\n";
    nossoTermoBool();
    tk = hf->nextToken();
    if (tk->category() == Token::tokenCateg::opBool){
        std::cout << "expBool (" << count << ") = expBool (";
        std::cout << nossoContador() << ") 'opBool' (" << tk->lexema().toStdString().c_str() << ") termoBool (";
        std::cout << nossoContador() << ")\n";
        nossaExpBool();
    } else {
        tk = hf->prevToken();
    }
}

void analisadorLexico::nossoTermoBool(){
    tk = hf->nextToken();

    if (tk->lexema() == "NOT"){
        std::cout << "termoBool (" << count << ") = 'prNot' (NOT) termoBool (";
        std::cout << nossoContador() << ")\n";
        nossoTermoBool();
    } else if (tk->category() != Token::tokenCateg::prFalse && tk->category() != Token::tokenCateg::prTrue){
        if (tk->category() == Token::tokenCateg::prFalse) {
            std::cout << "termoBool (" << count << ") = 'prFalse' (false) nossaExpRel (";
        } else if (tk->category() == Token::tokenCateg::prTrue) {
            std::cout << "termoBool (" << count << ") = 'prTrue' (true) nossaExpRel (";
        } else if (tk->category() == Token::tokenCateg::prId) {
            std::cout << "termoBool (" << count << ") = 'prId' (" << tk->lexema().toStdString().c_str()
                      << ") nossaExpRel (";
        } else {
            std::cout << "termoBool (" << count << ") = nossaExpRel (";
        }
        std::cout << nossoContador() << ")\n";
        tk = hf->prevToken();
        nossaExpRel();
    }
}

void analisadorLexico::nossaExpRel(){
    std::cout << "expRel (" << count << ") = expArit(";
    std::cout << nossoContador() << ")\n";
    nossaExpArit();
    tk = hf->nextToken();
    if (tk->category() == Token::tokenCateg::opRel){
        std::cout << "expRel (" << count << ") = expArit(";
        std::cout << nossoContador() << ") 'srRel' (" << tk->lexema().toStdString().c_str()
                  << ") expRel (" << nossoContador() << ")\n";
        nossaExpArit();
    }
}

void analisadorLexico::nossaExpArit(){
    tk = hf->nextToken();
    if (tk->category() != Token::tokenCateg::opUn){
        std::cout << "expArit (" << count << ") = expAritMult (";
        std::cout << nossoContador() << ")\n";
        tk = hf->prevToken();
    } else {
        std::cout << "expArit (" << count << ") = 'srUni' (--) expAritMult )";
        std::cout << nossoContador() << ")\n";
    }

    nossaExpAritMult();
    std::cout << "expArit (" << count << ") = expAritAd (";
    std::cout << nossoContador() << ")\n";
    nossaExpAritAd();
}

void analisadorLexico::nossaExpAritAd(){
    tk = hf->nextToken();
    if (tk->category() == Token::tokenCateg::opAd){
        std::cout << "expAritAd (" << count << ") = 'opAd' ("
                  << tk->lexema().toStdString().c_str() << ") expAritMult (";
        std::cout << nossoContador() << ")\n";
        nossaExpAritMult();
        std::cout << "expAritAd (" << count << ") = expAritAd (";
        std::cout << nossoContador() << ")\n";
        nossaExpAritAd();
    } else {
        std::cout << "expAritAd (" << count << ") = vazio \n";
        tk = hf->prevToken();
        // não tem operador aditivo
    }
}

void analisadorLexico::nossaExpAritMult(){
    std::cout << "expAritMult (" << count << ") = expAritExpo (";
    std::cout << nossoContador() << ")\n";
    nossaExpAritExpo();
    tk = hf->nextToken();
    if (tk->category() == Token::tokenCateg::opMult){
        std::cout << "expAritMult (" << count << ") = 'opMult' ("
                  << tk->lexema().toStdString().c_str() << ") expAritExpo (";
        std::cout << nossoContador() << ")\n";
        nossaExpAritExpo();
        std::cout << "expAritMult (" << count << ") = expAritMult (";
        std::cout << nossoContador() << ")\n";
        nossaExpAritAd();
    } else {
        std::cout << "expAritMult (" << count << ") = vazio\n";
        tk = hf->prevToken();
        // não tem operador multiplicativo
    }
}

void analisadorLexico::nossaExpAritExpo(){
    std::cout << "expAritExpo (" << count << ") = expAritNum (";
    std::cout << nossoContador() << ")\n";
    nossaExpAritNum();

    tk = hf->nextToken();
    if (tk->category() == Token::tokenCateg::opExp){
        std::cout << "expAritExpo (" << count << ") = 'opExp' ("
                  << tk->lexema().toStdString().c_str() << ") expAritNum (";
        std::cout << nossoContador() << ")\n";
        nossaExpAritExpo();
    } else {
        std::cout << "expAritExpo (" << count << ") = vazio\n";
        tk = hf->prevToken();
        // não tem operador multiplicativo
    }
}

void analisadorLexico::nossaExpAritNum(){
    tk = hf->nextToken();
    if (tk->category() == Token::tokenCateg::srParO){
        std::cout << "expAritNum (" << count << ") = expRel (";
        std::cout << nossoContador() << ")\n";
        nossaExpRel();
        tk = hf->nextToken();

        if (tk->category() != Token::tokenCateg::srParC){
            erro("falta )");
        }
    } else if (tk->category() == Token::tokenCateg::prId){
        std::cout << "expAritNum (" << count << ") = 'id' (" << tk->lexema().toStdString().c_str() << ")\n";
    } else if (tk->category() == Token::tokenCateg::cteFloat) {
        std::cout << "expAritNum (" << count << ") = 'cteFloat' (" << tk->lexema().toStdString().c_str() << ")\n";
    } else if (tk->category() == Token::tokenCateg::cteInt) {
        std::cout << "expAritNum (" << count << ") = 'cteInt' (" << tk->lexema().toStdString().c_str() << ")\n";
    } else {
        erro("era esperado um id ou valor");
    }
}

void analisadorLexico::erro(QString msgErro) {
    qDebug() << "----------------------------------------------------------";
    qDebug() << "ERRO:" << msgErro << "na posicao" << tk->line() << tk->colunm();
    qDebug() << "ultimo token lido: " << tk->lexema();
    qDebug() << "----------------------------------------------------------";
    exit(0);
}

int analisadorLexico::nossoContador()
{
    count = count + 1;
    return count;
}
