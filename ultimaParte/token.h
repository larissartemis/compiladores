#ifndef TOKEN_H
#define TOKEN_H

#include <QString>
#include <QDebug>

class Token
{
public:
    enum tokenCateg {
        opRel,
        opAd,
        opUn,
        opMult,
        opExp,
        opBool,
        concat,
        prCast,
        prInt,
        prLInt,
        prFloat,
        prChar,
        prStr,
        prBool,
        prArray,
        prVoid,
        prIf,
        prElse,
        prWhile,
        prFor,
        prRead,
        prWrite,
        prTrue,
        prFalse,
        prMain,
        srParO,
        srParC,
        srColO,
        srColC,
        srChaO,
        srChaC,
        srAtr,
        cteStr,
        cteChar,
        cteInt,
        cteFloat,
        srSc,
        srComma,
        prId,
        prIdFunc,
        invalid,
    };

    Token ();
    Token (int nLine, int nColumn, QString nLexema);
    ~Token ();

    void print();
    Token::tokenCateg identifier();

    int line();
    int colunm();
    tokenCateg category();
    QString lexema();

    void setLine(int nLine);
    void setColumn(int nColumn);
    void setCategory(tokenCateg nCategory);
    void setLexema(int nLexema);

private:
    int m_line, m_column;
    tokenCateg m_category;
    QString m_lexema;
};

#endif // TOKEN_H
