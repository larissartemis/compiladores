#ifndef ANALISADORLEXICO_H
#define ANALISADORLEXICO_H

#include "token.h"
#include "handlefile.h"

class analisadorLexico
{
public:
    analisadorLexico(QString arquivo);

    void nossoS();
    void nossoLs();
    void nossoIf();
    void nossoElse();
    void nossoCast();
    void nossoWrite();
    void nossoRead();
    void nossoWhile();
    void nossoFor();
    void nossoConcat();
    void nossoValor();
    void nossaAtr();
    void nossoAtrArray();
    void nossaExpBool();
    void nossoTermoBool();
    void nossaExpRel();
    void nossaExpArit();
    void nossaExpAritAd();
    void nossaExpAritMult();
    void nossaExpAritExpo();
    void nossaExpAritNum();
    void nossoMain();
    void nossoAtrTipo();
    void nossoTipo();
    void nossoVarValor();

    void erro(QString msgErro);
    int nossoContador();
public:
    Token * tk;
    handleFile * hf;
    int count = 0;
};

#endif // ANALISADORLEXICO_H
