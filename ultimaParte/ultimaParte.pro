QT += core
QT -= gui

CONFIG += c++11

TARGET = ultimaParte
CONFIG += console
CONFIG -= app_bundle

TEMPLATE = app

SOURCES += main.cpp \
    analisadorlexico.cpp \
    token.cpp \
    handlefile.cpp

HEADERS += \
    analisadorlexico.h \
    token.h \
    handlefile.h

DISTFILES += \
    todo.txt \
    fibonacci.txt \
    helloWorld.txt \
    shellSort.txt \
    ssOutput.txt \
    hwOutput.txt \
    fibonacciOutput.txt
